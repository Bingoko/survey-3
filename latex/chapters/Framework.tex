% !TEX root = ../FnTIR2021-Rec.tex

%\section{Traditional Recommendation with \ac{VAE}-based methods}
\chapter{Hyperparameter Optimization Framework}
\label{sec:framework}

Hyperparameter optimization is a time-consuming process. Especially for models with a large number of hyperparameters such as deep learning, the training process usually takes dozens of hours. \cite{thornton2012auto} was the first to verify the feasibility of using a fully automatic method \citep{hall2009weka} to select learning algorithms and their hyperparameters. Later, many open source tools were proposed to reduce the threshold for developers by combining optimization algorithms and schedulers. This section will discuss those popular open source libraries or frameworks. Table \ref{tab:framework} shows hyperparameter optimization framework. 

\section{Scikit-learn}

Scikit-learn \citep{pedregosa2011scikit} (also known as sklearn) is a free software machine learning library for the Python programming language. It has various classification, regression and clustering algorithms, including support vector machine, random forest, gradient boosting, k-means. Sklearn provides two general hyperparameter optimization methods: grid search and random search. It uses cross-validation to effectively evaluate the performance of each configuration, and achieves parallel computing.
The library can be found at: \url{http://scikit-learn.sourceforge.net}.

\section{HyperOpt}

HyperOpt \citep{bergstra2013hyperopt} is a powerful python library for hyperparameter optimization. It provides two hyperparameter optimization methods: random search and BO-TPE method. By working with distributed training algorithms, hyperOpt can be used to generate trial runs with different hyperparameter settings on the driver node. Hyperopt can be combined with sklearn to form HyperOpt-sklearn \citep{komer2014hyperopt} libraries. 
The library can be found at: \url{http://jaberg.github.com/hyperopt}.


\section{BayesOpt}

BayesOpt \citep{martinez2014bayesopt} is a library that uses Bayesian optimization methods to solve nonlinear optimization, random bandit, or sequential experimental design problems. It uses the Gaussian process as a surrogate model and uses prior knowledge to determine the subsequent results.
The library can be found at: \url{https://bitbucket.org/rmcantin/bayesopt/}

\section{Google Vizier}

Google vizier \citep{golovin2017google} provides an internal Google service for performing black box optimization. Its biggest feature is its ease of use. Users only need to select a relevant search algorithm and submit a configuration file, and then they will get a suggested set of hyperparameters. Vizier's modular design makes it easy to support multiple algorithms.
The library can be found at: \url{https://github.com/tobegit3hub/advisor}

\section{Ray Tune}

Ray tune \citep{liaw2018tune} introduces a scalable hyperparameter optimization Python library.It uses the distributed hyperparameter search of multiple computing nodes and supports multiple deep learning frameworks, such as: pytorch, TensorFlow. 
Tune is available at \url{http://ray.readthedocs.io/en/latest/tune.html}. 

\section{GPflowOpt}

GPflowOpt \citep{knudde2017gpflowopt} introduces a new Python framework for Bayesian optimization. It uses TensorFlow to make Gaussian process tasks run on the GPU. Therefore, the running speed under this framework is faster and more efficient. However, the framework still lacks advanced sampling techniques such as batch processing BO, and its performance on discrete variables needs to be verified. 
The library can be found at: \url{https://github.com/GPflow/GPflowOpt}

\section{Skopt}

Skopt(scikit-optimize) \citep{head2018scikit} is a hyperparameter optimization framework built on the scikit-learn library to minimize expensive and noisy black box functions. It includes models such as random search, Bayesian search, decision forest and gradient boosting tree. 
The library can be found at: \url{https://scikit-optimize.github.io/stable/}

\section{Autotune}

Autotune \citep{koch2018autotune} provides an automatic parallel non-derivative hyperparameter optimization framework. It shows that parallel training in the model is better than the experimental results obtained by single training. In addition, it can be applied to almost all hyperparameter types in black box optimization, which makes its applicability stronger. 

\section{Optuna}

Optuna \citep{akiba2019optuna} provides a Bayes-based method for hyperparameter optimization and effective search structuring. It has the following functions.

\begin{itemize}
	\setlength{\itemsep}{0pt}
	\setlength{\parsep}{0pt}
	\setlength{\parskip}{0pt}
	\item imperative, define-by-run style API
	\item lightweight, versatile and cross-platform architecture
	\item efficient sampling algorithms and pruning algorithms
\end{itemize}

Therefore, the code written in Optuna has a high degree of modularity, and users of Optuna can also dynamically construct the search space of hyperparameters.
The library can be found at: \url{https://github.com/pfnet/optuna/}.

\begin{table}
	%\newcommand{\tabincell}[2]{\begin{tabular}{@{}#1@{}}#2\end{tabular}}
	\caption{Paper Query Results}
	\label{tab:framework}       % Give a unique label	
	% For LaTeX tables use	
	\resizebox{\textwidth}{!}{
		\begin{threeparttable}
			\begin{tabular}{llll}		
				\hline\noalign{\smallskip}		
				\textbf{Framework} & \textbf{Optimization Method} & \textbf{Feature} \\		
				\noalign{\smallskip}\hline\noalign{\smallskip}		
				Scikit-learn & Grid search and random search & - Cross-validation \\
				\noalign{\smallskip}\hline
				HyperOpt & Random Search and BO-TPE & \tabincell{l}{- ability to create \\complex parameter spaces\\- Persisting and restarting\\- Parallelization}  \\
				\noalign{\smallskip}\hline
				BayesOpt & \tabincell{l}{Bayesian Optimization with\\ a linear regression model} & - Balance of explore and exploit \\
				\noalign{\smallskip}\hline
				Google Vizier & \tabincell{l}{- Suitable for many\\ algorithms} & \tabincell{l}{- Easy to use \\- Early stopping strategy}  \\
				\noalign{\smallskip}\hline
				Ray tune & \tabincell{l}{- Suitable for many\\ algorithms} & \tabincell{l}{- Supports many machine\\ learning framework\\- Visualized results\\-  distributed hyperparameter sweep} \\
				\noalign{\smallskip}\hline
				GPflowOpt & BO-GP & \tabincell{l}{- Parallelization\\- Suitable for GPU} \\
				\noalign{\smallskip}\hline
				Skopt & BO-GP & \tabincell{l}{- Simple and efficient}\\
				\noalign{\smallskip}\hline
				Autotune & Hybrid Solver Manager & \tabincell{l}{- Suitable for multiple\\ hyperparameters\\- Parallelization}  \\
				\noalign{\smallskip}\hline
				Optuna & Bayes-based method & \tabincell{l}{- Parallelization\\- Quick visualization\\- Lightweight, versatile, \\and platform-agnostic architecture\\- efficient sampling algorithms \\and pruning algorithms}  \\
				
				\noalign{\smallskip}\hline
			\end{tabular}
			
	\end{threeparttable}}
\end{table}